﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SolarLab.BusManager.Abstraction;
using SolarLab.BusManager.Implementation;
using Microsoft.Extensions.Hosting;
using SevSharks.Identity.WebUI.Configurations;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.HttpOverrides;

namespace SevSharks.Identity.WebUI
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        /// <summary>
        /// IConfiguration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// IConfiguration
        /// </summary>
        public IWebHostEnvironment Environment { get; }

        /// <summary>
        /// ConfigureServices
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddResponseCompression()
                .AddHttpContextAccessor()
                .AddLocalization(options => options.ResourcesPath = "Resources");

            services
                .AddControllersWithViews()
                .AddRazorRuntimeCompilation()
                .AddViewLocalization()
                .AddDataAnnotationsLocalization()
                .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            services.AddDb(Configuration);
            // var rsa = new RsaKeyService(Environment, TimeSpan.FromDays(30));
            // services.AddSingleton(provider => rsa);
            services.AddIdentityServerForSevShark(Configuration, Environment);
            services.AddSevSharksAuthentication();
            services.AddSevSharksMapper();

            services.AddSingleton(Configuration);
            services.AddSingleton((IConfigurationRoot) Configuration);
            services.AddSingleton<IBusManager, MassTransitBusManager>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            var forwardedHeaderOptions = new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto |
                                   ForwardedHeaders.XForwardedHost
            };
            forwardedHeaderOptions.KnownNetworks.Clear();
            forwardedHeaderOptions.KnownProxies.Clear();

            app.UseForwardedHeaders(forwardedHeaderOptions);

            var supportedCultures = new[] { "ru", "en" };
            var localizationOptions = new RequestLocalizationOptions()
            {
                ApplyCurrentCultureToResponseHeaders = true
            }
                .SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);
            app.UseRequestLocalization(localizationOptions);

            app.UseStaticFiles()
                .UseRouting()
                .UseResponseCompression()
                .UseAuthentication()
                .UseHttpsRedirection()
                .UseAuthorization()
                .UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute())
                .UseIdentityServer();
        }
    }
}