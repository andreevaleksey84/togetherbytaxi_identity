﻿using System.Configuration;

namespace SevSharks.Identity.WebUI.Configurations
{
    public class SevSharksIdentityConfigurationConstants
    {
        public const string MigrationAssemblyName = "SevSharks.Identity.DataAccess";
    }
}
