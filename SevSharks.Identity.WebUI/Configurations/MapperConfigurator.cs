﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using SevSharks.Identity.BusinessLogic.MapProfiles;

namespace SevSharks.Identity.WebUI.Configurations
{
    public static class MapperConfigurator
    {
        public static IServiceCollection AddSevSharksMapper(this IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CreateUserServiceMapProfiles>();
                cfg.AddProfile<UserMapProfile>();
            });

            configuration.AssertConfigurationIsValid();

            return configuration;
        }
    }
}
