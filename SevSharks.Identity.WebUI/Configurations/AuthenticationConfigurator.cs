﻿using IdentityServer4;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using SevSharks.Identity.WebUI.ExternalAuthentication.VkConnection;
using System.Security.Claims;

namespace SevSharks.Identity.WebUI.Configurations
{
    public static class AuthenticationConfigurator
    {
        public static IServiceCollection AddSevSharksAuthentication(this IServiceCollection services)
        {
            /*
                                            //.AddGoogle("Google", options =>
                                //{
                                //    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                                //    options.ClientId = "1042223585808-6vn7hjihdj189rhhpfp7sd4ps232lf83.apps.googleusercontent.com";
                                //    options.ClientSecret = "0YsEyRveKcUExq7km84LdKfX";
                                //})
                                //.AddFacebook("facebook", options =>
                                //{
                                //    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                                //    options.ClientId = "1939113896168784";
                                //    options.AppSecret = "6b7fe1719ace991ef20c2ae01ebd4153";
                                //})

             */
            /*
.AddOK("Одноклассники", options =>
{
    options.ClientId = "1273085952";
    options.PublicApplicationKey = "CBABMCOMEBABABABA";
    options.ClientSecret = "A751011563E78F41740122F9";
    options.SignInScheme = IdentityConstants.ExternalScheme;

    // Request for permissions email to Odnoklassniki API team
    options.Scope.Add("VALUABLE_ACCESS");
    options.Scope.Add("email");

    // Add fields https://apiok.ru/dev/methods/rest/users/users.getCurrentUser
    options.Fields.Add("uid");
    options.Fields.Add("first_name");
    options.Fields.Add("last_name");
    options.Fields.Add("email");

    // In this case email will return in OAuthTokenResponse, 
    // but all scope values will be merged with user response
    // so we can claim it as field
    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "uid");
    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
    options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
    options.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "first_name");
    options.ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
});
*/
            services
                .AddAuthentication()
                .AddVk("VK", options =>
                {
                    // andreevaleksey84@list.ru
                    // https://vk.com/editapp?id=8119518&section=options
                    options.ApiVersion = "8.57";
                    options.ClientId = "8119518";
                    // сервисный ключ доступа c0af39e3c0af39e3c0af39e350c0d4dd3dcc0afc0af39e3a2ed86a7a02b48bd8da9e985
                    // Защищённый ключ 7ivhCDBZaANjzwLiigQV
                    // указывать Защищённый ключ
                    options.ClientSecret = "7ivhCDBZaANjzwLiigQV";
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    // Request for permissions https://vk.com/dev/permissions?f=1.%20Access%20Permissions%20for%20User%20Token
                    options.Scope.Add("email");

                    // Add fields https://vk.com/dev/objects/user
                    options.Fields.Add("uid");
                    options.Fields.Add("first_name");
                    options.Fields.Add("last_name");

                    // In this case email will return in OAuthTokenResponse, 
                    // but all scope values will be merged with user response
                    // so we can claim it as field
                    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "uid");
                    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
                    options.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "first_name");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
                })
                .AddGoogle("Google", options =>
                {
                    // andreevaleksey84@gmail.com
                    // https://console.cloud.google.com/apis/credentials?project=togetherbytaxi
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = "210960008869-2bd9kijb55sshjuaerefpp40dmts8bpr.apps.googleusercontent.com";
                    options.ClientSecret = "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";
                })
                .AddMicrosoftAccount(options =>
                {
                    // aleksey.andreev1984@hotmail.com
                    // https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Overview/appId/6866a494-c3ae-4430-83c8-e8e8aa1e9c7d/objectId/6f4d8b12-fb31-4fe5-b069-e4e08e5d70ed/isMSAApp~/false/defaultBlade/Overview/appSignInAudience/AzureADandPersonalMicrosoftAccount/servicePrincipalCreated~/true
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = "6866a494-c3ae-4430-83c8-e8e8aa1e9c7d";
                    options.ClientSecret = "1lb8Q~gPGPDxR61T0vEGhpZ7_Q7mqeeoCPqnYaYt";
                });
            return services;
        }
    }
}
