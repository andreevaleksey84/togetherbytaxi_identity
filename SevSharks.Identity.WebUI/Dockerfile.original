FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 5000
EXPOSE 44350

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY SevSharks.Identity.WebUI/SevSharks.Identity.WebUI.csproj SevSharks.Identity.WebUI/
COPY SevSharks.Identity.DataAccess/SevSharks.Identity.DataAccess.csproj SevSharks.Identity.DataAccess/
COPY SevSharks.Identity.BusinessLogic/SevSharks.Identity.BusinessLogic.csproj SevSharks.Identity.BusinessLogic/
RUN dotnet restore SevSharks.Identity.WebUI/SevSharks.Identity.WebUI.csproj
COPY . .
WORKDIR /src/SevSharks.Identity.WebUI
RUN dotnet build SevSharks.Identity.WebUI.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish SevSharks.Identity.WebUI.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "SevSharks.Identity.WebUI.dll"]
