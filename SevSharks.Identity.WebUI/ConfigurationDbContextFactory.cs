﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using IdentityServer4.EntityFramework.Options;
using IdentityServer4.EntityFramework.DbContexts;

namespace SevSharks.Identity.WebUI
{
    public class ConfigurationDbContextFactory : IDesignTimeDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            var migrationAssembly = "SevSharks.Identity.DataAccess";
            optionsBuilder.UseNpgsql("ConnectionStrings_DefaultConnection: \"Host=localhost;Port=5432;Database=sevsharks_auth;UserId=postgres;Password=password\"", b => b.MigrationsAssembly(migrationAssembly));

            var t = new ConfigurationStoreOptions();
            return new ConfigurationDbContext(optionsBuilder.Options, t);
        }
    }
}
