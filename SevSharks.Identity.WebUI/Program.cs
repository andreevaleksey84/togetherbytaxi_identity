﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using SolarLab.BusManager.Abstraction;
using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

namespace SevSharks.Identity.WebUI
{
    /// <summary>
    /// Основной старт выполнения
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Основной старт выполнения
        /// </summary>
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            var services = host.Services;
            var logger = services.GetRequiredService<ILogger<Program>>();
            logger.LogInformation("Auth started");
            try
            {
                var busManager = services.GetService<IBusManager>();
                // busManager.StartBus(ServiceBusConfigurator.GetBusConfigurations(services)); //TODO: run this to start MassTransit listening
            }
            catch (Exception ex)
            {
                logger.LogError("Error during start bus for Auth", ex);
            }

            // Seed data
            SeedData.EnsureSeedData(services);

            host.Run();
        }

        /// <summary>
        /// CreateWebHostBuilder
        /// </summary>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                    .UseSerilog((context, configuration) =>
                    {
                        configuration.ReadFrom.Configuration(context.Configuration);
                        configuration.Enrich.FromLogContext();
                    })
                   .ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                        config.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true);
                        config.AddJsonFile("secrets/appsettings.secrets.json", optional: true);
                        config.AddEnvironmentVariables();
                        config.AddCommandLine(args);
                    })
                    .ConfigureLogging((hostingContext, logging) =>
                    {
                        logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        logging.AddSerilog();
                        logging.AddConsole();
                        logging.AddDebug();
                        logging.AddEventSourceLogger();
                    })
                   .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
                   .UseDefaultServiceProvider(configure =>
                   {
                       configure.ValidateOnBuild = true;
                       configure.ValidateScopes = true;
                   });
    }
}
