﻿namespace SevSharks.Identity.WebUI.Models.Captcha
{
    /// <summary>
    /// Класс валидации капчи
    /// </summary>
    public class ValidCaptcha
    {
        /// <summary>
        /// Показтель успешности валидации
        /// </summary>
        public bool Success { get; set; }
    }
}
