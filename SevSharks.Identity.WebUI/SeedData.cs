﻿using IdentityServer4;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using SevSharks.Identity.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using SevSharks.Identity.BusinessLogic;
using SevSharks.Identity.BusinessLogic.Models;

namespace SevSharks.Identity.WebUI
{
    /// <summary>
    /// </summary>
    public class SeedData
    {
        /// <summary>
        ///     scopes define the resources in your system
        /// </summary>
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new CustomIdentityResources.Roles(),
                new CustomIdentityResources.Permissions()
            };
        }

        /// <summary>
        ///     GetApiResources
        /// </summary>
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new CustomApiResources.SignalRApiResource(),
                new CustomApiResources.OrderApiResource()
            };
        }

        /// <summary>
        ///     GetApiResources
        /// </summary>
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope(name: CustomApiResources.CustomScopes.SignalrScopeName,   displayName: CustomApiResources.CustomScopes.SignalrScopeName),
                new ApiScope(name: CustomApiResources.CustomScopes.OrderScopeName,   displayName: CustomApiResources.CustomScopes.OrderScopeName),
            };
        }

        /// <summary>
        ///     clients want to access resources (aka scopes)
        /// </summary>
        public static IEnumerable<Client> GetClients()
        {
            const int accessTokenLifeTime = 7200;
            const int identityTokenLifeTime = 7200;

            // client credentials client
            return new List<Client>
            {
                // JavaScript Client
                new Client
                {
                    ClientId = Constants.WebSpaClientId,
                    ClientName = Constants.WebSpaClientName,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,

                    RedirectUris =
                    {
                        "http://localhost:4200/login/callback", "http://localhost:4200/assets/silent-renew.html",
                        "http://localhost:5008/login/callback", "http://localhost:5008/assets/silent-renew.html",
                        "http://webspa/login/callback", "http://webspa/assets/silent-renew.html",
                        "http://togetherbytaxi.ru:5008/login/callback", "http://togetherbytaxi.ru:5008/assets/silent-renew.html",
                        "http://togetherbytaxi.ru/login/callback", "http://togetherbytaxi.ru/assets/silent-renew.html",
                        "https://togetherbytaxi.ru/login/callback", "https://togetherbytaxi.ru/assets/silent-renew.html",
                    },
                    PostLogoutRedirectUris = {"http://localhost:4200", "http://localhost:5008", "http://webspa", "http://togetherbytaxi.ru:5008", "http://togetherbytaxi.ru", "https://togetherbytaxi.ru", },
                    AllowedCorsOrigins = {"http://localhost:4200", "http://localhost:5008", "http://webspa", "http://togetherbytaxi.ru:5008", "http://togetherbytaxi.ru", "https://togetherbytaxi.ru", },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        CustomIdentityResources.CustomScopes.Roles,
                        CustomIdentityResources.CustomScopes.Permissions,
                        CustomApiResources.CustomScopes.SignalrScopeName,
                        CustomApiResources.CustomScopes.OrderScopeName
                    },

                    AccessTokenLifetime = accessTokenLifeTime,
                    IdentityTokenLifetime = identityTokenLifeTime
                },
                // Client for test
                new Client
                {
                    ClientId = Constants.ClientTestId,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess = true,
                    Enabled = true,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        CustomIdentityResources.CustomScopes.Roles,
                        CustomIdentityResources.CustomScopes.Permissions,
                        CustomApiResources.CustomScopes.SignalrScopeName,
                        CustomApiResources.CustomScopes.OrderScopeName
                    },

                    AccessTokenLifetime = accessTokenLifeTime,
                    IdentityTokenLifetime = identityTokenLifeTime
                }
            };
        }

        /// <summary>
        ///     EnsureSeedData
        /// </summary>
        public static void EnsureSeedData(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger("SeedData");
            logger.LogInformation("Start migrate for PersistedGrantDbContext");
            scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            logger.LogInformation("End migrate for PersistedGrantDbContext");

            var configurationDbContext = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            logger.LogInformation("Start migrate for ConfigurationDbContext");
            configurationDbContext.Database.Migrate();
            logger.LogInformation("End migrate for ConfigurationDbContext");

            var anyClients = configurationDbContext.Clients.Any();
            logger.LogInformation("ConfigurationDbContext has Clients = {clientsExist}", anyClients);
            if (!anyClients)
            {
                logger.LogInformation("ConfigurationDbContext does not have Clients. Adding clients");
                foreach (var client in GetClients())
                {
                    logger.LogInformation("ConfigurationDbContext add Client. {client}", client);
                    configurationDbContext.Clients.Add(client.ToEntity());
                }

                configurationDbContext.SaveChanges();
            }

            if (!configurationDbContext.IdentityResources.Any())
            {
                foreach (var resource in GetIdentityResources())
                {
                    configurationDbContext.IdentityResources.Add(resource.ToEntity());
                }

                configurationDbContext.SaveChanges();
            }

            if (!configurationDbContext.ApiResources.Any())
            {
                foreach (var resource in GetApiResources())
                {
                    configurationDbContext.ApiResources.Add(resource.ToEntity());
                }

                configurationDbContext.SaveChanges();
            }


            if (!configurationDbContext.ApiScopes.Any())
            {
                foreach (var apiScope in GetApiScopes())
                {
                    configurationDbContext.ApiScopes.Add(apiScope.ToEntity());
                }

                configurationDbContext.SaveChanges();
            }

            var context = scope.ServiceProvider.GetService<Context>();
            context.Database.Migrate();
            var createUserService = scope.ServiceProvider.GetService<CreateUserService>();

            AddAllUsers(createUserService, logger);
        }

        private static void AddAllUsers(CreateUserService createUserService, ILogger logger)
        {
            try
            {
                logger.LogInformation("Adding all users");
                CreateUser(createUserService, logger, "1@mail.ru");
                CreateUser(createUserService, logger, "2@mail.ru");
            }
            catch (Exception e)
            {
                logger.LogError(e ,"Error during adding users");
            }
        }

        private static void CreateUser(CreateUserService createUserService, ILogger logger, string userName)
        {
            logger.LogInformation("Add user {userName}", userName);
            var userDto = new CreateUserDto
            {
                UserName = userName,
                EmailConfirmed = true,
                PhoneNumber = "+79780256699",
                PhoneNumberConfirmed = true,
                Password = "Pass123$",
                Email = userName
            };

            var addedUser = createUserService.CreateUser(userDto).Result;
            logger.LogInformation("Added user {userName}", addedUser.UserName);
        }
    }
}