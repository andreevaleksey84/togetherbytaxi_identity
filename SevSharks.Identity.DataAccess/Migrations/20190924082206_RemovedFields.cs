﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SevSharks.Identity.DataAccess.Migrations
{
    public partial class RemovedFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BonusAccountBalance",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BonusAccountNumber",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IdFromIt1",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BonusAccountBalance",
                table: "Users",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "BonusAccountNumber",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdFromIt1",
                table: "Users",
                nullable: true);
        }
    }
}
